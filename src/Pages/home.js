import Carousel from 'react-elastic-carousel';
import './pages.css'
import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import SearchIcon from '@mui/icons-material/Search';
import { styled } from '@mui/material/styles';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  marginLeft:'25%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

function Slider() {
  
  const [data, setData] = React.useState([]);
  const[searchTerm,setSearchTerm]=React.useState('');
  const [filterParam, setFilterParam] = React.useState('All');
  const[cart,setCart]=React.useState([]);
  // const Home =(handleClick)=>{
  
  React.useEffect(() => {
    fetch("/product")
      .then((res) => res.json())
     // .then((data)=> setData(data.product));
       .then((data) => setData(data.result));
        
       
  }, []);
  React.useEffect(() => {
    fetch("/cart")
      .then((res) => res.json())
     // .then((data)=> setData(data.product));
       .then((cart) => setCart(cart.cart));
        
       
  }, []);

  
  const handleClick = (id, quantity) =>{
    const blog = { id, quantity };

    fetch('http://localhost:3001/cart', {
      method: 'POST',
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(blog)
    }).then(() => {
      console.log(blog);
      window.location.reload();
    })
   
  }

 
  
  // const [x,setx]=React.useState([null]);
  //  setx(data);
  
 

  const   items= [
      {id: 1, title: 'All' },
      {id: 2, title: 'Pizza'},
      {id: 3, title: 'Burger'},
      {id: 4, title: 'Rice'},
      {id: 5, title: 'Dessert'},
      {id: 6, title: 'Soft drink'},
      {id: 7, title: 'Noodles'},
      {id: 8, title: 'Koththu'},
      {id: 9, title: 'Sea food'}
]

 
    return (
        <div>
        <div className="carousel">

      <Carousel itemsToShow={6}>
        
        {items.map(item => <div key={item.id}>
          <button className="slider-btn-item" value={item.title} onClick={event=>{setFilterParam(event.target.value);}}>
          {item.title}
          </button>
          </div>)}
      </Carousel>

      </div>
      <div className="search">
      <input 
      type="text" 
      className="searchTerm" 
      placeholder="search ur favourite dish...." 
      onChange={event =>{
        setSearchTerm(event.target.value);
        }}
        
        /><SearchIconWrapper>
              <SearchIcon style={{color:"black"}}/>
            </SearchIconWrapper>
          
   </div>

      <div className="item-card">
        
      <div style={{ display: "flex", flexWrap: "wrap"}}>
        
      {data.filter((postData)=>{
       
        if(searchTerm===""&& filterParam=="All"){
         
          return postData;
        }
        else if(postData.productName.toLowerCase().includes(searchTerm.toLowerCase())&&filterParam=="All"){

          return postData;
        }
        else if(filterParam==postData.productType){
         
          
          if(searchTerm===""){
            return postData;
          }
          else if(postData.productName.toLowerCase().includes(searchTerm.toLowerCase())){
            console.log(searchTerm);
            return postData;
          }
        }
        
        
      })
    .map((postData) => {
        
								
                // postData=JSON.stringify(postData).toString();
               
                
								return (
                  // <div key={postData.productid}>
                  
        <div className="card" key={postData.productid}>
          
        
      <Card sx={{ maxWidth: 345 }}>
      <CardMedia
        component="img"
        height="160px"
        width="100%"
        src={"/Images/"+postData.imagesrc}
        alt="pizza"
      />
      <CardContent style={{ padding:0 }}>
        <Typography gutterBottom variant="h5" component="div">
        <p style={{ padding:0, margin:0 }} className="product-name">{postData.productName}</p>
        
        <p style={{ padding:0, margin:0 }} className="price">Rs. {postData.productPrice}</p>
        </Typography>
      </CardContent>
      <CardActions>
        <Button variant="contained" color="success" style={{ marginLeft:10 }} onClick={()=> handleClick(postData.productid,1)} >Add to Card</Button>
        <Link style={{ textDecoration:'none' }} to={{
    pathname: "/detail",
    search: "?sort=name",
    hash: "#the-hash",
    state: { id:postData.productid }
  }}><Button variant="outlined" color="error" style={{ marginLeft:45 }} >Details</Button></Link>
      </CardActions>
    </Card>
  
    </div>
						
    // </div>
    	);
    })}
    </div>
    </div>
   

   
				
         
    </div>
    )

  
  
}



//}  
 

export default Slider

