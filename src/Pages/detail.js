import * as React from 'react';
import {useLocation} from 'react-router-dom';
import './detail.css'
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';

const Detail=()=>{

  const [data, setData] = React.useState([]);
  const[searchTerm,setSearchTerm]=React.useState('');
  const [filterParam, setFilterParam] = React.useState('All');
  const[cart,setCart]=React.useState([]);
  // const Home =(handleClick)=>{
  
  React.useEffect(() => {
    fetch("/product")
      .then((res) => res.json())
     // .then((data)=> setData(data.product));
       .then((data) => setData(data.result));
        
       
  }, []);
    
  const handleClick = (id, quantity) =>{
    const blog = { id, quantity };

    fetch('http://localhost:3001/cart', {
      method: 'POST',
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(blog)
    }).then(() => {
      console.log(blog);
      window.location.reload();
    })
   
  }

const location = useLocation()

//store the state in a variable if you want 
//location.state then the property or object you want
// console.log(location.state.id)

const Id = location.state.id

return(
<div className='border-boxx'>
         {data.map((ele)=> {
               if(Id==ele.productid){ 
                console.log(ele.productid);
           return(
            
          <div className="outerboc" key={ele.productid}>
            
                <div className="inner_box" key={ele.productid}>
                <div class="name">
               <span>{ele.productName}</span>
            </div>
             <div className='cart_img'>
               <img src={"/Images/"+ele.imagesrc} alt=""/>
             </div>
             <div className="des">
             
             <div>
               <span>Price: {ele.productPrice}</span>
            </div>
            <div>
               <span>Type: {ele.productType}</span>
            </div>
            <div>
               <span>Description: {ele.description}</span>
            </div>
            <CardActions>
            <Button variant="contained" color="success" style={{ marginLeft:10 }} onClick={()=> handleClick(ele.productid,1)} >Add to Card</Button>
            </CardActions>
            </div>
            </div>
          </div>
             
           );
          }
         })}
         

  {/* <div>
    hello my name is {Id}
  </div> */}
  </div>
)

}
    
  
  export default Detail