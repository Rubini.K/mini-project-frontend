import React, { useEffect } from 'react'
import './cart.css'
function Cart({cart,setCart}) {
    const [price, setPrice] = React.useState(0);
    const [product, setProduct] = React.useState([]);

    React.useEffect(() => {
      fetch("/cart")
        .then((res) => res.json())
       // .then((data)=> setData(data.product));
         .then((cart) => setCart(cart.cart));
          
         
    }, []);
    React.useEffect(() => {
      fetch("/product")
        .then((res) => res.json())
       // .then((data)=> setData(data.product));
         .then((product) => setProduct(product.result));
         
        
    }, []);

    const handleRemove = (productid) =>{
      // const arr=cart.filter((item)=> item.id !== productid);
      // setCart(arr);
      // handlePrice();
      fetch('/cart/'+productid, { method: 'DELETE' })
        .then(() => fetch("/cart")
        .then((res) => res.json())
       // .then((data)=> setData(data.product));
         .then((cart) => setCart(cart.cart))
         );
        
    }
    const handleChange = (id, d) =>{
      // const ind = cart.indexOf(item);
      // const arr = cart;
      // arr[ind].amount +=d;
  
      // if(arr[ind].amount ===0) arr[ind].amount=1;
      // setCart([...arr]);
  
    };

    const newcart = () =>{
      // document.getElementById('cart_boxx').style.display = "block";
  
    };

    const handlePrice = () => {
      let ans=0;
      cart.map((ele)=> {
        product.map((item)=>{
          if(ele.id==item.productid){
          ans += ele.quantity * item.productPrice}
        });
      });
      setPrice(ans);
    };

    useEffect(()=>{
      handlePrice();
    });
    var i=1;
    // console.log(cart);
    
    return (
      
       <div className='border-box'>
         
         {/* <div class="article">
              <div className="cart_box_outer">
              
              <div className="cart_boxx" >
                <button class="cartno">cart{i}</button>
              </div>

              <div className="cart_boxx" >
                <button class="cartno">cart2</button>
              </div>
              <div className="cart_boxx" >
                <button class="cartno">cart3</button>
              </div>
              
              <div className="cart_boxx" >
                <button class="cartno" onClick={() => newcart()}>create new cart</button>
              </div>
              </div>
              </div>
         <div class="article"> */}
         {cart.map((ele)=> {
                
          
           return(
            
          <div className="cart_box_outer" key={ele.id}>
            {product.map((item)=> {
              if(item.productid==ele.id){
                console.log(item.productid);
              return (
                
                <div className="cart_box" key={item.productid}>
             <div className='cart_img'>
               <img src={"/Images/"+item.imagesrc} alt=""/>
               <p>{item.productName}</p>
             </div>
             <div>
               <button onClick={() => handleChange(ele.id,1)}>+</button>
               <button>{ele.quantity}</button>
               <button onClick={() => handleChange(ele.id,-1)}>-</button>
           </div>
             <div>
               <span>{item.productPrice}</span>
               <button onClick={() => handleRemove(ele.id)}>Remove</button>
            </div>
            </div>
             );}
            })}
          </div>
             
           );
         })}
         <div className='total'>
           <span>Total Price of your Cart</span>
          <span>Rs. {price}</span>
         </div>
       </div>
      //  </div>
       
       //<div>Cart</div>
    );
   
}
  export default Cart