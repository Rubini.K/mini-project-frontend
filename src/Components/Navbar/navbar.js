import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MoreIcon from '@mui/icons-material/MoreVert';
import { Button } from '@mui/material';
import "./navbar.css"
import { lightBlue } from '@mui/material/colors';
import SvgIcon from '@mui/material/SvgIcon';
import 'bootstrap';
import Select from '@mui/material/Select';
import DeleteIcon from '@mui/icons-material/Delete';





export default function PrimarySearchAppBar({ setShow, size }) {
  const [data, setData] = React.useState([]);
  React.useEffect(() => {
    fetch("/cart/carts")
      .then((res) => res.json())
     // .then((data)=> setData(data.product));
       .then((data) => setData(data.cart));
        
       
  }, []);
  
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  var x=1;
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = 'primary-search-account-menu';

  const create = (id,name) =>{
    const blog = { id,name };

    fetch('http://localhost:3001/cart/carts', {
      method: 'POST',
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(blog)
    }).then(() => {
      console.log(blog);
      window.location.reload();

    })
    // x++;
   
  }

  const remove = (id) =>{
    // const arr=cart.filter((item)=> item.id !== productid);
    // setCart(arr);
    // handlePrice();
    fetch('/cart/carts/'+id, { method: 'DELETE' })
      .then(() => fetch("/cart/carts")
      .then((res) => res.json())
     // .then((data)=> setData(data.product));
       .then((data) => setData(data.cart))
       );
      
  }
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
     
    
        <Button href='/'>
          Home
        </Button>
        
        
        
      </MenuItem>
      <MenuItem>
      <Button href='/'>
          Home
        </Button>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );
//   var i=0
//  var arr;
//   for(i=1;i<x;i++){
//     arr[i]=i;
//   }
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" className="toolbar-box" style={{ backgroundColor: '#020331' }}>
        <Toolbar>
        <img id="nav-logo" src="https://images.assetsdelivery.com/compings_v2/wikagraphic/wikagraphic2010/wikagraphic201037373.jpg" alt="nav-logo" className="img-fluid d-inline-block align-baseline me-2" />
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ display: { xs: 'none', sm: 'block' } }}
            className="restaurant-name"
            onClick={() => setShow(true)}>
           RK Restaurant
          </Typography>
          
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
            
            <Button href='/' >
            <HomeIcon sx={{ color: lightBlue[50], fontSize: 40 }} />
        </Button>
        
          <Select className="select">
          {data.map((ele)=> {
            x=ele.id;
            x=x+1;
           return(
  <MenuItem><Button href='/cart'>{ele.name}{ele.id} </Button><Button onClick={() => remove(ele.id)}><DeleteIcon></DeleteIcon></Button></MenuItem>
  );
  
          })}
{/* <MenuItem><Button href='/cart'>Cart1</Button></MenuItem> */}
  <MenuItem ><Button onClick={()=> create(x,'Cart')}>Create New</Button></MenuItem>
</Select>

        <Button href='/about' class="navbar-button">
         About
        </Button>
        <Button href='/login' class="navbar-button">
          Login
        </Button>
           
          </Box>
          <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </Box>
  );
}
function HomeIcon(props) {
    return (
      <SvgIcon {...props}>
        <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
      </SvgIcon>
    );
  }