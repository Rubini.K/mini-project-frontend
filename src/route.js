import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Navbar from './Components/Navbar/navbar'
import Home from './Pages/home'
import About from './Pages/about'
import Dashboard from './Pages/dashboard'
import Cart from './Pages/cart'
import Detail from './Pages/detail'


export default function BasicExample() {
  const[cart,setCart]=React.useState([]);
  const [show, setShow] = React.useState(true);

  React.useEffect(() => {
    fetch("/cart")
      .then((res) => res.json())
     // .then((data)=> setData(data.product));
       .then((cart) => setCart(cart.cart));
        
       
  }, []);

  

  

  return (
      <div>
          <Navbar setShow={setShow} size={cart.length}/>
    <Router>
      
       

        
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/login">
            <Dashboard />
          </Route>
          <Route path="/cart">
            <Cart cart={cart} setCart={setCart}/>
            {/* <Cart /> */}
          </Route>
          <Route path="/detail">
            <Detail />
          </Route>
        </Switch>
     
    </Router>
    </div>
  );
}






